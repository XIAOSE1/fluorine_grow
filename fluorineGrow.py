"""
Create possible fluorine grow volumns for a complex based on Relibase result.
Update: Aug., 2018
Contact: sean.xiao@novartis.com
Presets: copy two *.brix file, two *.mol2 file, and this python script to a local directory
Presets: update below directory with your local directory
Presets: add one line in pymolrc: run yourDirectory\fluorineGrow.py
Usage: fluorinegrow object
"""
#load 3gvu.pdb, protein1
#cmd.remove("resi 1002")
#remove resi 1002

import datetime, sys, time, os
import pymol
from pymol import cmd, stored

def fluorinegrow(obj):
	
	cmd.remove("solvent")
	cmd.remove("e. h")
	#obj = cmd.get_object_list('polymer')
	#obj_name = obj[0]
	obj_name = str(obj)
	print (obj_name)

	cmd.select("organics", "not polymer")
	stored.nplist = []
	cmd.iterate ("organics", "stored.nplist.append((resi,resn, ID))")
	print (stored.nplist)
	for i in range(len(stored.nplist)):
		id = stored.nplist[i][2]
		atoms = cmd.count_atoms("byres ID "+str(id)) 
		if atoms < 6: 
			cmd.remove("ID "+str(id))
	
	cmd.select("pro.n.atom", "(byres organic expand 5 )and "+obj_name+" and n. n and not solvent")
	stored.nlist = []
	cmd.iterate ("pro.n.atom", "stored.nlist.append((resi,resn,ID))")
	print (stored.nlist)
	
	cmd.select("pro.o.atom", "(byres organic expand 5 )and "+obj_name+" and n. o and not solvent")
	stored.olist = []
	cmd.iterate ("pro.o.atom", "stored.olist.append((resi,resn,ID))")
	print (stored.olist)
	
	cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\n.dist.mol2","lig.n")
	cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\o.dist.mol2","lig.o")
	cmd.disable("lig.n")
	cmd.disable("lig.o")
	
	cmd.select("lig.n.", "lig.n & e. n extend 1")
	cmd.select("lig.o.", "lig.o & e. o extend 2")
	
	###############
	for n in range(len(stored.nlist)):
		print stored.nlist[n][2]
		id = stored.nlist[n][2]
		cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\N_dist_empty.brix","n.m."+str(id))
		cmd.select("n.pro.sel."+str(id), obj_name+" & ID "+str(id)+" extend 1")
		cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\n.dist.mol2","n.lig."+str(id))
		cmd.select("lig.nme", "n.lig."+str(id)+" & e. n extend 1")
		cmd.pair_fit("lig.nme", "n.pro.sel."+str(id))
		cmd.show("sticks","n.pro.sel."+str(id))
		cmd.enable('n.lig.'+str(id),1)
		cmd.matrix_transfer("n.lig."+str(id),"n.m."+str(id))
		cmd.disable('n.lig.'+str(id))
		#volume vol_name, map_name, ramp, selection
		#cmd.volume("n.v."+str(id),"n.m."+str(id),"fofc")
		#volume_color vol_name, 3 blue 0 3.3 blue 0.05 3.6 blue 0 3.65 green 0 3.975 green 0.2 4.3 green 0 4.35 red 0 4.675 red 0.8 5 red 0
		ramp = "3 blue 0 3.3 blue 0.05 3.6 blue 0 3.65 green 0 3.975 green 0.2 4.3 green 0 4.35 red 0 4.675 red 0.8 5 red 0"
		select1 = "((organic & "+obj_name+" around 3.5 ) & ! (organic & "+obj_name+" around 1.3 ) ) & ! resn Nme & n.lig."+str(id)
		select11 = select1+" & ! byres ((byres "+select1+") & ! "+select1+")"
		select2 = "((n.pro.sel."+str(id)+" around 3.5 ) & ! (n.pro.sel."+str(id)+" around 3 ) ) & ! resn Nme & n.lig."+str(id)
		#select22 = select2+" & ! byres ((byres "+select2+") & ! "+select2+")"
		select22 = "byres "+select2 
		select = "n.lig."+str(id)+" and ("+select11+" and "+select22+")"
		cmd.select("n.lig.sel."+str(id),select) 
		#cmd.select("point to LMW: 1.3-3.5; point to protein: 3-3.5","all")
		if cmd.count_atoms("n.lig.sel."+str(id)) > 0 : 
			cmd.volume("n.v."+str(id),"n.m."+str(id), ramp, select) 
			cmd.delete("lig.nme") 
			#cmd.group("n."+str(id),"n.m."+str(id)+" n.v."+str(id)+" n.pro.sel."+str(id)+" n.lig."+str(id))
			cmd.group("n."+str(id),"n.*."+str(id)) 
		else: 
			cmd.delete("n.m."+str(id)) 
			cmd.delete("n.pro.sel."+str(id)) 
			cmd.delete("n.lig."+str(id)) 
			cmd.delete("n.lig.sel."+str(id)) 
			cmd.delete("lig.nme")
	
	for o in range(len(stored.olist)):
		print stored.olist[o][2]
		id = stored.olist[o][2]
		cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\O_dist_empty.brix","o.m."+str(id))
		cmd.select("o.pro.sel."+str(id),obj_name+" & ID "+str(id)+" extend 2")
		cmd.load("C:\\Users\\xiaose1\\Desktop\\fluorine_growing\\o.dist.mol2","o.lig."+str(id))
		cmd.select("lig.ome", "o.lig."+str(id)+" & e. o extend 2")
		cmd.pair_fit("lig.ome", "o.pro.sel."+str(id))
		cmd.show("sticks","o.pro.sel."+str(id))
		cmd.enable('o.lig.'+str(id),1)
		cmd.matrix_transfer("o.lig."+str(id),"o.m."+str(id))
		cmd.disable('o.lig.'+str(id))
		ramp = "3 blue 0 3.3 blue 0.05 3.6 blue 0 3.65 green 0 3.975 green 0.2 4.3 green 0 4.35 red 0 4.675 red 0.8 5 red 0"
		select1 = "((organic & "+obj_name+" around 3.5 ) & ! (organic & "+obj_name+" around 1.3 ) ) & ! resn Ome & o.lig."+str(id)
		select11 = select1+" & ! byres ((byres "+select1+") & ! "+select1+")"
		select2 = "((o.pro.sel."+str(id)+" around 3.5 ) & ! (o.pro.sel."+str(id)+" around 3 ) ) & ! resn Ome & o.lig."+str(id)
		select22 = "byres "+select2 
		select = "o.lig."+str(id)+" and ("+select11+" and "+select22+")"
		cmd.select("o.lig.sel."+str(id),select) 
		if cmd.count_atoms("o.lig.sel."+str(id)) > 0 : 
			cmd.volume("o.v."+str(id),"o.m."+str(id),ramp, select)
			cmd.delete("lig.ome")
			cmd.group("o."+str(id),"o.*."+str(id))
		else: 
			cmd.delete("o.m."+str(id)) 
			cmd.delete("o.pro.sel."+str(id)) 
			cmd.delete("o.lig."+str(id)) 
			cmd.delete("o.lig.sel."+str(id)) 
			cmd.delete("lig.ome")
	
	cmd.zoom("organic & "+obj_name) 
	cmd.scene("new","store","fluorine_grow: click to show N...F interactions\n or O...F interactions")

cmd.extend("fluorinegrow",fluorinegrow)