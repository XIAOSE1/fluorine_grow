# fluorine_grow

# Create possible fluorine grow volumns for a complex based on Relibase result.
# Update: Aug., 2018
# Contact: sean.xiao@novartis.com
# Presets: copy two *.brix file, two *.mol2 file, and this python script to a local directory
# Presets: update below directory with your local directory
# Presets: add one line in pymolrc: run yourDirectory\fluorineGrow.py
# Usage: fluorinegrow object